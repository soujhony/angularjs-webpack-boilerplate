export default class MenuController {
    constructor() {
        this.itens = [
            {
                state:'app.dashboard',
                icon: 'icon-home',
                name: 'Dashboard'
            },{
                state:'app.cliente',
                icon: 'icon-users',
                name: 'Clientes'
            },{
                state:'app.produto',
                icon: 'icon-shopping-cart',
                name: 'Produtos'
            },{
                state:'app.pedido',
                icon: 'icon-file',
                name: 'Pedidos'
            }
        ]
    }
}